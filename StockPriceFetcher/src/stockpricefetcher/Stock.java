/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package stockpricefetcher;

import java.math.BigDecimal;

/**
 *
 * @author 1795845
 */
public class Stock {

    int id;
    String symbol;
    String name;
    BigDecimal price;

    public Stock() {
    }

 

    public Stock(int id, String symbol, String name, BigDecimal price) {
        this.id = id;
        this.symbol = symbol;
        this.name = name;
        this.price = price;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getSymbol() {
        return symbol;
    }

    public void setSymbol(String symbol) {
        this.symbol = symbol;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }
  @Override
    public String toString() {
        return "Stock{" + "id=" + id + ", symbol=" + symbol + ", name=" + name + ", price=" + price + '}';
    }
}
