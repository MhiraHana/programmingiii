/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package stockpricefetcher;


import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;


/**
 *
 * @author 1795845
 */
public class Database {

    /*user name tododb*/
    private final static String HOSTNAME = "localhost:3333";
    private final static String DBNAME = "stockpricefetcher";
    private final static String USERNAME = "stockpricefetcher";
    private final static String PASSWORD = "vwqCYV6VUvS9Qcs4";

    private Connection conn;

    public Database() throws SQLException {
        conn = DriverManager.getConnection("jdbc:mysql://" + HOSTNAME + "/" + DBNAME, USERNAME, PASSWORD);

    }

    public void addTodo(Stock stock) throws SQLException {
        String sql = "INSERT INTO stock (name, symbol, price) VALUES (?, ?, ?)";
        try (PreparedStatement stmt = conn.prepareStatement(sql)) {
            stmt.setString(1, stock.getName());
            stmt.setString(2, stock.getSymbol());
            stmt.setString(3, stock.getPrice().toPlainString());

            stmt.executeUpdate();
        }

    }

    public ArrayList<Stock> getAllStocks() throws SQLException {
        String sql = "SELECT * FROM stock";
        ArrayList<Stock> list = new ArrayList<>();
        try (Statement stmt = conn.createStatement()) {
            ResultSet result = stmt.executeQuery(sql);
            while (result.next()) {
                Stock stock = new Stock();
                stock.id = result.getInt("id");
                stock.symbol = result.getString("symbol");
                stock.name = result.getString("name");
                stock.price=result.getBigDecimal("price");
               // stock.price = new BigDecimal(result.getString("price"));// NumberFormatException
                
                list.add(stock);
            }
        } /*catch (NumberFormatException e) {

            throw new SQLException("Error parsing price field", e);

        }*/
        return list;

    }

    public void addStock(Stock stock) throws SQLException {
        String sql = "INSERT INTO stock (name, symbol, price) VALUES (?, ?, ?)";
        try (PreparedStatement stmt = conn.prepareStatement(sql)) {
            stmt.setString(1, stock.getName());
            stmt.setString(2, stock.getSymbol());
           //stmt.setString(3, stock.getPrice().toPlainString());
            stmt.setBigDecimal(3, stock.getPrice());

            stmt.executeUpdate();
        }

    }

    public void updateStock(Stock stock) throws SQLException {
        String sql = "UPDATE stock SET name=?, symbol=?, price=? WHERE id=?";

        PreparedStatement statement = conn.prepareStatement(sql);
        statement.setString(1, stock.getName());

        statement.setString(2, stock.getSymbol());
        statement.setString(3, stock.getPrice().toPlainString());
        //statement.setBigDecimal(3, stock.getPrice());
        statement.setInt(4, stock.getId());
        statement.executeUpdate();

    }

    public void deleteStockById(int id) throws SQLException {
        String sql = "DELETE FROM stock WHERE id=?";

        PreparedStatement statement = conn.prepareStatement(sql);
        statement.setInt(1, id);
        statement.executeUpdate();

    }
}
