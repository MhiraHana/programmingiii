/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package traveldb;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

/**
 *
 * @author Hana
 */
public class Database {
    private final static String HOSTNAME = "localhost:3333";
    private final static String DBNAME = "traveldb";
    private final static String USERNAME = "traveldb";
    private final static String PASSWORD = "sAEbud8KYS3cTDzX";
  
    /*maison 8SxDCqxSXsveEqtp*/
     private Connection conn;

    public Database() throws SQLException {
        conn = DriverManager.getConnection("jdbc:mysql://" + HOSTNAME + "/" + DBNAME, USERNAME, PASSWORD);

    }
       
    public void addTrip(Trip trip) throws SQLException {
        String sql = "INSERT INTO trip (destination, name, passportNo, departure, returner) VALUES (?, ?, ?, ?, ?)";
        try (PreparedStatement stmt = conn.prepareStatement(sql)) {
            stmt.setString(1, trip.getDestination());
            stmt.setString(2, trip.getName());
            stmt.setString(3, trip.getPasseportNo());
            stmt.setDate(4, trip.getDepartureDateSql());
            stmt.setDate(5, trip.getReturnDateSql());
            
            stmt.executeUpdate();
        }
    }
    
    public ArrayList<Trip> getAllTrips() throws SQLException {
        String sql = "SELECT * FROM trip";
        ArrayList<Trip> list = new ArrayList<>();
        
        try (Statement stmt = conn.createStatement()) {
            ResultSet result = stmt.executeQuery(sql);
            while (result.next()) {
                int id = result.getInt("id");
                String destination = result.getString("destination");
                String name = result.getString("name");
                String passportNo = result.getString("passeportNo");
                java.sql.Date departureDateSql = result.getDate("departure");
                java.sql.Date returnDateSql = result.getDate("returner");
                Trip trip = new Trip(id,destination, name, passportNo, departureDateSql, returnDateSql);
                list.add(trip);
            }
        }
        return list;
    }
    
    public Trip getTripById(int id) throws SQLException {
        throw new RuntimeException("TODO: method not implemented");
    }
    
    public void updateTrip(Trip trip) throws SQLException {
        throw new RuntimeException("TODO: method not implemented");
    }
    
    public void deleteTripById(int id) throws SQLException {
        throw new RuntimeException("TODO: method not implemented");
    }
    
}
