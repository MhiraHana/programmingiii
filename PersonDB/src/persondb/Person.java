/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package persondb;

import java.sql.Date;
import java.text.ParseException;
import java.text.SimpleDateFormat;

/**
 *
 * @author Hana
 */
public class Person {

    int id;
    String name;
    Date birthday;
    boolean gender;
    String emailAddress;

    public Person() {
    }

    public Person(int id, String name, String birthday, boolean gender, String emailAddress) {
        this.id = id;
        this.name = name;
        setBirthday(birthday);
        this.gender = gender;
        this.emailAddress = emailAddress;
    }

    public Person(int id, String name, java.sql.Date birthday, String gender, String emailAddress) {
        this.id = id;
        this.name = name;
        setBirthday(birthday);
        setGender(gender);
        this.emailAddress = emailAddress;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    private SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");

    public Date getBirthday() {

        return birthday;
    }

    public String getBirthdayString() {
        return dateFormat.format(birthday);

    }

    public void setBirthday(String birthdayStr) {
        try {
            birthday = (Date) dateFormat.parse(birthdayStr);
        } catch (ParseException ex) {
            throw new IllegalArgumentException("Due Date invaslid");
        }
    }

    public void setBirthday(Date birthday) {
        this.birthday = birthday;
    }

    public String isGenderString() {
        return gender ? "Male" : "Female";
    }

    public boolean isGender() {
        return gender;
    }

    public final void setGender(String genderString) {
        switch (genderString) {
            case "done":
                gender = true;
                break;
            case "pending":
                gender = false;
                break;
            default:
                throw new IllegalArgumentException("isDone must be done or pending, not " + genderString);

        }
    }

    public void setGender(boolean gender) {
        this.gender = gender;
    }

    public String getEmailAddress() {
        return emailAddress;
    }

    public void setEmailAddress(String emailAddress) {
        this.emailAddress = emailAddress;
    }

    @Override
    public String toString() {
        return "Person{" + "id=" + id + ", name=" + name + ", birthday=" + birthday + ", gender=" + gender + ", emailAddress=" + emailAddress + '}';
    }

}
