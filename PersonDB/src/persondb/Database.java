/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package persondb;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

/**
 *
 * @author Hana
 */
public class Database {

    //zXDdhqpUwnMADyXq
    private final static String HOSTNAME = "localhost:3306";
    private final static String DBNAME = "persondb";
    private final static String USERNAME = "persondb";
    private final static String PASSWORD = "zXDdhqpUwnMADyXq";
    private Connection conn;

    public Database() throws SQLException {
        conn = DriverManager.getConnection("jdbc:mysql://" + HOSTNAME + "/" + DBNAME, USERNAME, PASSWORD);

    }

    public void addPerson(Person person) throws SQLException {
        String sql = "INSERT INTO person (name, birthday, gender, emailAddress) VALUES (?, ?, ?,?)";
        try (PreparedStatement stmt = conn.prepareStatement(sql)) {
            stmt.setString(1, person.getName());
            stmt.setDate(2, person.getBirthday());
            stmt.setString(3, person.isGenderString());
            stmt.setString(4, person.getEmailAddress());
            stmt.executeUpdate();
        }

    }

    public ArrayList<Person> getAllTPersons() throws SQLException {
        String sql = "SELECT * FROM person";
        ArrayList<Person> list = new ArrayList<>();
        try (Statement stmt = conn.createStatement()) {
            ResultSet result = stmt.executeQuery(sql);
            while (result.next()) {
                int id = result.getInt("id");
                String name = result.getString("name");
                java.sql.Date brithdayDateSql = result.getDate("brithday");
                String genderStr = result.getString("gender");
                String emailAddress = result.getString("emailAddress");

                Person person = new Person(id, name, brithdayDateSql, genderStr, emailAddress);
                list.add(person);
            }
            return list;
        }
    }

    public Person getPersonById(int id) throws SQLException {
        String sql = "SELECT * FROM person WHERE id=" + id;

        try (Statement stmt = conn.createStatement()) {
            ResultSet result = stmt.executeQuery(sql);
            if (result.next()) {

                String name = result.getString("name");
                java.sql.Date brithdayDateSql = result.getDate("brithday");
                String genderStr = result.getString("gender");
                String emailAddress = result.getString("emailAddress");
                Person person = new Person(id, name, brithdayDateSql, genderStr, emailAddress);
                return person;
            } else {
                throw new SQLException("Record not found");
            }

        }

    }

    public void updatePerson(Person person) throws SQLException {
        String sql = "UPDATE person SET name=?, brithday=?, gender=?, emailAddress=? WHERE id=?";

        PreparedStatement stmt = conn.prepareStatement(sql);
        stmt.setString(1, person.getName());
        stmt.setDate(2, person.getBirthday());
        stmt.setString(3, person.isGenderString());
        stmt.setString(4, person.getEmailAddress());
        stmt.executeUpdate();

    }

    public void deletePersonById(int id) throws SQLException {
        String sql = "DELETE FROM person WHERE id=?";

        PreparedStatement statement = conn.prepareStatement(sql);
        statement.setInt(1, id);
        statement.executeUpdate();

    }
}
