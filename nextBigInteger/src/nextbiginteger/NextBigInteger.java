/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package nextbiginteger;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Arrays;

/**
 *
 * @author Hana
 */
public class NextBigInteger {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws IOException {
        // TODO code application logic here
        BufferedReader br= new BufferedReader(new InputStreamReader(System.in));
     System.out.println("Enter an integer value");
     int n=Integer.parseInt(br.readLine());
     NextBigInteger.big(n);
     System.out.println();
    }
   static void big(int a)
 {
   String n=a+"";
   char c[]=n.toCharArray();
   for(int i=c.length-1;i>=1;i--)
   {
       if (c[i-1]>c[i])
       {
           continue;
       }
       else
       {
         //swap
           int index=NextBigInteger.check(c,c[i-1]);
           char temp=c[i-1];
           c[i-1]=c[index];
           c[index]=temp;
           
           Arrays.sort(c,i,c.length);
          
           break;
       }
       
   }
   NextBigInteger.print(c);
 }
 static void print(char c[])
 {
     for(int m=0;m<c.length;m++)
           {
           System.out.print(c[m]);
           }
 }
 static int check(char a[],int j)
 {
     int k;
         for( k=a.length-1;k>=0;k--)
         {
           if(a[k]>j)
            break; 
         }    
         return k;
 }
 
 
}
