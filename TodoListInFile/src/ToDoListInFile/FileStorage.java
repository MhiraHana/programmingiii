/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ToDoListInFile;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.InputMismatchException;
import java.util.Scanner;

/**
 *
 * @author Hana
 */
public class FileStorage {

    public static final String FILE_NAME = "data.txt";

    static void saveToDoToFile(ArrayList<ToDo> list) throws IOException {
//FIXME: use try-with-resources here 
        // Assume default encoding.
        try (PrintWriter out = new PrintWriter(new BufferedWriter(new FileWriter(FILE_NAME)))) {
            for (ToDo d : list) {
                out.println(d.getTask());
                out.println(d.getDueDate());
                out.println(d.isIsDone());

            }
        }

    }

    static ArrayList<ToDo> loadToDoFromFile() throws IOException, ParseException {
        ArrayList<ToDo> list = new ArrayList<>();
        try {
            File file = new File(FILE_NAME);
            if (!file.exists()) {
                return list;
            }
            Scanner in = new Scanner(file); // scanner open file

            while (in.hasNextLine()) {
                String task = in.nextLine();
                String dueDate = in.nextLine();
                String str = in.nextLine();
                boolean isDone = Boolean.parseBoolean(str);
                ToDo d = new ToDo(task, dueDate, isDone);
                list.add(d);
            }
        } catch (InputMismatchException e) {
            throw new IOException("Error while parsig data file", e);

        } catch (IllegalArgumentException e) {

            throw new IOException("Error while parsig data file", e);
        }
        return list;
    }

}
