/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ToDoListInFile;

import java.text.ParseException;
import java.text.SimpleDateFormat;

/**
 *
 * @author Hana
 */
public class ToDo {

    private String task; // 1-100 characters long
    private String dueDate; // valid date in yyyy-mm-dd format
    private boolean isDone;

    public ToDo(String task, String dueDate, boolean isDone) throws ParseException {
        setTask(task);
        setDueDate(dueDate);
        this.isDone = isDone;
    }

    public String getTask() {
        return task;
    }

    public final void setTask(String task) {
        if (task.length() < 1 || task.length() > 100) {

            throw new IllegalArgumentException("Task must 1-100 characters long");
        }

        this.task = task;
    }

    public String getDueDate() {
        return dueDate;
    }

    public final void setDueDate(String dueDate) throws ParseException {

        if (!isValidDate(dueDate)) {

            throw new IllegalArgumentException("You must enter string in this format yyyy-MM-dd ");
        }

        this.dueDate = dueDate;
    }

    public boolean isIsDone() {
        return isDone;
    }

    public final void setIsDone(boolean isDone) {
        this.isDone = isDone;
    }

    @Override
    public String toString() {

        return String.format("%s is at %s and it is  %s", task, dueDate, isDone);

    }

    public static boolean isValidDate(String inDate) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        dateFormat.setLenient(false);
        try {
            dateFormat.parse(inDate.trim());

        } catch (ParseException pe) {
            return false;
        }
        return true;
    }
}
