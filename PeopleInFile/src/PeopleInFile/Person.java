/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package PeopleInFile;

/**
 *
 * @author 1795845
 */
public class Person {

   private String name;
   private  int age;
    //TODO: enum Gender

    /*In Class Person:
    **add getters and setters
    ** In setters require:
    -name is not null and between 2 and 50 caracters long
    - age is 0-150
    *If value given to a setter is not valid then throw an IllegalArgumentException ( do not create your own exception)
    with message describing the proble 
    
    *Implement a constructor that tkes 2 parameters (name, age) then users setters to set the values
    
    
    throw new IllegalArgumentException("Raison why");
    throws
     */
    public Person(String name, int age) {
        setName(name);
        setAge(age);


    }

    public String getName() {

        return name;

    }

    public int getAge() {

        return age;
    }

    /*setters*/
    public final void setName(String name) {

        if (name== null) {

            throw new IllegalArgumentException("Name must not be null");

        } 
        if (name.length() < 2 || name.length() > 50) {

            throw new IllegalArgumentException("Name must 2-50 characters long");
        } 
           
        this.name = name;

        
    }

    public final void setAge(int age) {
        if (age < 0 || age > 150) {
            throw new IllegalArgumentException("Age must be between 0-15");
        }   this.age = age;
        

    }
    
   @Override
   public String toString(){
   
   return String.format("%s is %d y/o", name,age);
   
   }
}
