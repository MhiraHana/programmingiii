/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bestcar;

import java.util.Comparator;

/**
 *
 * @author 1795845
 */
public class Car implements Comparable<Car> {

    String make; // e.g. BMW, Toyota, etc.
    String model; // e.g. X5, Corolla, etc.
    int maxSpeedKmph; // maximum speed in km/h
    double secTo100Kmph; // accelleration time 1-100 km/h in seconds
    double litersPer100km; // economy: liters of gas per 100 km
    static int count = 0;

    public Car(String make, String model, int maxSpeedKmph, double secTo100Kmph, double litersPer100km) {
        this.make = make;
        this.model = model;
        this.maxSpeedKmph = maxSpeedKmph;
        this.secTo100Kmph = secTo100Kmph;
        this.litersPer100km = litersPer100km;
    }

    public String getMake() {
        return make;
    }

    public String getModel() {
        return model;
    }

    public int getMaxSpeedKmph() {
        return maxSpeedKmph;
    }

    public double getSecTo100Kmph() {
        return secTo100Kmph;
    }

    public double getLitersPer100km() {
        return litersPer100km;
    }

    public static int getCount() {
        return count++;
    }

    @Override
    public String toString() {
        return "Car{" + "make=" + make + ", model=" + model + ", maxSpeedKmph=" + maxSpeedKmph + ", secTo100Kmph=" + secTo100Kmph + ", litersPer100km=" + litersPer100km + '}';
    }

    @Override
    public int compareTo(Car compareCar) {
        int comparemaxSpeedKmph = ((Car) compareCar).getMaxSpeedKmph();
        /* For Ascending order*/
        return this.maxSpeedKmph - comparemaxSpeedKmph;

    }
    public static Comparator<Car> CarNameComparator = new Comparator<Car>() {

        @Override
        public int compare(Car c1, Car c2) {

            String CarName1 = c1.getModel().toUpperCase();
            String CarName2 = c2.getModel().toUpperCase();

            //ascending order
            return CarName1.compareTo(CarName2);

            //descending order
            //return fruitName2.compareTo(fruitName1);
        }

    };
    public static Comparator<Car> CarComparatorByMaxSpeed = new Comparator<Car>() {

        @Override
        public int compare(Car c1, Car c2) {
            int CarAcceleration1 = c1.getMaxSpeedKmph();
            int CarAcceleration2 = c2.getMaxSpeedKmph();

            //ascending order
            return (int) (CarAcceleration1 - CarAcceleration2);
        }

    };

    public static Comparator<Car> CarComparatorByAccelleration = new Comparator<Car>() {

        @Override
        public int compare(Car c1, Car c2) {
            int CarAcceleration1 = c1.getMaxSpeedKmph();
            int CarAcceleration2 = c2.getMaxSpeedKmph();

            //ascending order
            return (int) (CarAcceleration1 - CarAcceleration2);
        }

    };
    public static Comparator<Car> CarComparatorByEconomy = new Comparator<Car>() {

        @Override
        public int compare(Car c1, Car c2) {
            double CarEconomy1 = c1.getLitersPer100km();
            double CarEconomy2 = c2.getLitersPer100km();

            //ascending order
            return (int) (CarEconomy1 - CarEconomy2);
        }

    };

}
