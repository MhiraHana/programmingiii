/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bestcar;

import java.util.ArrayList;
import java.util.Collections;

/**
 *
 * @author 1795845
 */
public class BestCar {

    static ArrayList<Car> garage = new ArrayList<>();

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        System.out.println(Car.getCount());
        // public Car(String make, String model, int maxSpeedKmph, double secTo100Kmph, double litersPer100km)
        Car c1 = new Car("BMW", "X5", 10, 20.2, 20.3);
        System.out.println(Car.getCount());
        Car c2 = new Car("Toyota", "Corolla", 10, 20.2, 20.3);
        System.out.println(Car.getCount());
        Car c3 = new Car("Honda", "Honda", 10, 20.2, 20.3);
        System.out.println(Car.getCount());
        Car c4 = new Car("Nissan", "Nissan", 10, 20.2, 20.3);
        System.out.println(Car.getCount());
        Car c5 = new Car("Audi", "Audi A3", 10, 20.2, 20.3);
        System.out.println(Car.getCount());
        Car c6 = new Car("Jaguar", "Jaguar", 10, 20.2, 20.3);
        System.out.println(Car.getCount());
        Car c7 = new Car("Kia", "Kia", 10, 20.2, 20.3);
        System.out.println(Car.getCount());
        garage.add(c1);
        garage.add(c2);
        garage.add(c3);
        garage.add(c4);
        garage.add(c5);
        garage.add(c6);
        garage.add(c7);
        System.out.println();
        System.out.println();
        System.out.println("*****************Before sorting *************************");

        for (Car c : garage) {
            System.out.printf("%s is %s has %d, %.2f and %.2f \n", c.make, c.model, c.maxSpeedKmph, c.secTo100Kmph, c.litersPer100km);
        }
        System.out.println();
        System.out.println("*****************After  sorting by maxSpeedKmph *************************");

        Collections.sort(garage);

        for (Car str : garage) {
            System.out.println(str);
        }
        System.out.println();
        System.out.println("*****************After  sorting by CarNameComparator *************************");

        Collections.sort(garage, Car.CarNameComparator);

        for (Car str : garage) {
            System.out.println(str);
        }
        System.out.println();
        System.out.println("*****************After  sorting by CarComparatorByAccelleration *************************");

        Collections.sort(garage, Car.CarComparatorByAccelleration);

        for (Car str : garage) {
            System.out.println(str);
        }
        System.out.println();
        System.out.println("*****************After  sorting by CarComparatorByAccelleration *************************");

        Collections.sort(garage, Car.CarComparatorByEconomy);

        for (Car str : garage) {
            System.out.println(str);
        }
    }

}
