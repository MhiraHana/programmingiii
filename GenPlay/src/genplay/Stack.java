/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package genplay;



/**
 *
 * @author Hana
 */
public class Stack<T> {

    private final int size;

    private int top;

    private T[] elements;

    public Stack() {
        this(10);
    }

    public Stack(int s) {
        size = s > 0 ? s : 10;
        top = -1;

        elements = (T[]) new Object[size]; // create array
    }

    public void push(T pushValue) {
        if (top == size - 1) // if stack is full
        {
            throw new FullStackException(String.format("Stack is full, cannot push %s", pushValue));
        }

        elements[++top] = pushValue; // place pushValue on Stack
    }

    public T pop() {
        if (top == -1) // if stack is empty
        {
           throw new EmptyStackException("Stack is empty, cannot pop");
        }

        return elements[top--]; // remove and return top element of Stack
    }

    public int getHeight() {
        return size;
    }
}
class EmptyStackException extends RuntimeException {
  public EmptyStackException() {
    this("Stack is empty");
  }

  public EmptyStackException(String exception) {
    super(exception);
  }
}

class FullStackException extends RuntimeException {
  public FullStackException() {
    this("Stack is full");
  }

  public FullStackException(String exception) {
    super(exception);
  }
}