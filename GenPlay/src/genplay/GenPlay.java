/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package genplay;

/**
 *
 * @author Hana
 */
public class GenPlay {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        Stack<Car> stackOfCars = new Stack<>();
	Stack<String> stackOfStrings = new Stack<>();
	
	System.out.printf("Stack of cars is %d tall, strings is %d tall\n", 
		stackOfCars.getHeight(), stackOfStrings.getHeight());
		
	stackOfCars.push(new Car("BMW", "X5", 10, 20.2, 20.3));
	stackOfCars.push(new Car("Toyota", "Corolla", 15, 18.2, 10.3));

	System.out.printf("Stack of cars is %d tall, strings is %d tall \n", 
	stackOfCars.getHeight(), stackOfStrings.getHeight());
         System.out.println();
	Car c2 = stackOfCars.pop();
        System.out.println(c2.toString());
	Car c1 = stackOfCars.pop();
        System.out.println(c1.toString());
	Car c0 = stackOfCars.pop(); // should return null, no more cars left
        System.out.println(c0.toString());
    }
    
}
