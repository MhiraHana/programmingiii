/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package enumperson;

/**
 *
 * @author 1795845
 */
public class Person {
    String name;
    Gender gender;
    AgeRange ageRange;

    public Person(String name, Gender gender, AgeRange ageRange) {
        this.name = name;
        this.gender = gender;
        this.ageRange = ageRange;
    }

    Person() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Gender getGender() {
        return gender;
    }

    public void setGender(Gender gender) {
        this.gender = gender;
    }

    public AgeRange getAgeRange() {
        return ageRange;
    }

    public void setAgeRange(AgeRange ageRange) {
        this.ageRange = ageRange;
    }

    //Jerry is a Male From18to35

    @Override
    public String toString() {
        return String.format("%s is a %s and %s", name, gender,ageRange);
    }
}
