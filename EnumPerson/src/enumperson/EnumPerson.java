/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package enumperson;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Scanner;

/**
 *
 * @author 1795845
 */
public class EnumPerson {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        try {
            File f = new File("people.txt");
            Scanner sc = new Scanner(f);

            ArrayList<Person> people = new ArrayList<>();

            while (sc.hasNextLine()) {
                String line = sc.nextLine();
                try {
                    String[] data = line.split(";");
                    if (data.length != 3) {
                        throw new ParsingException("Invalid number of fields, 3 expected");
                    }

//   if(details.length !=3){}
                    String name = data[0];
                    String genderStr = data[1];
                    String ageStr = data[2];
                    Gender gender = Gender.valueOf(genderStr);
                    AgeRange age = AgeRange.valueOf(ageStr);
                    Person p = new Person(name, gender, age);
                    people.add(p);
                } catch (ParsingException | IllegalArgumentException e) {
                    System.out.println("Error parsing line: " + line);
                    System.out.println("  Reason: " + e.getMessage());
                }
            }
        
        for (Person p : people) {
            System.out.println(p.toString());
        }

    } catch (IOException e) {
            /// e.printStackTrace();
            System.out.println("File reading error: " + e.getMessage());
        }
//    catch (FileNotFoundException ex
//
//    
//        ) {
//            ex.printStackTrace();
//    }
}


}

class ParsingException extends Exception {

    public ParsingException() {
        super();
    }

    public ParsingException(String message) {
        super(message);
    }

    public ParsingException(String message, Throwable cause) {
        super(message, cause);
    }
}
