/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package shoppingcart.Filter;

/**
 *
 * @author Hana
 */
public enum PromotionType {
    MARKED_PRICE,
    BUY_ONE_GET_ONE,
    BUY_THREE_FOR_PRICE_OF_TWO,
}