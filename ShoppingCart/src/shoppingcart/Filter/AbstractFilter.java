/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package shoppingcart.Filter;

import shoppingcart.Exception.ItemNotSameTypeException;
import java.util.HashSet;
import java.util.List;
import java.util.stream.Collectors;
import shoppingcart.Item;

/**
 *
 * @author Hana
 */
public abstract class AbstractFilter {
    public abstract double filterPrice(List<Item> items) throws ItemNotSameTypeException;

    public void filterItemsBeforeCalculatePrice(List<Item> items) throws ItemNotSameTypeException {
        HashSet<String> itemNames = items.stream()
                .map(Item::getName)
                .collect(Collectors.toCollection(HashSet::new));

        if (itemNames.size() > 1) {
            throw new ItemNotSameTypeException("Items are not same type");
        }
    }
}
