/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package shoppingcart.Filter;

import java.util.List;
import shoppingcart.Exception.ItemNotSameTypeException;
import shoppingcart.Item;

/**
 *
 * @author Hana
 */
public class MarkedPriceFilter extends AbstractFilter {

    public double filterPrice(List<Item> items) throws ItemNotSameTypeException {
        super.filterItemsBeforeCalculatePrice(items);

        if (items.size() == 0) {
            return 0;
        }

        return items.size() * items.stream().findFirst().get().getPrice();
    }
}
