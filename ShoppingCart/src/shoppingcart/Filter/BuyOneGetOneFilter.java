/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package shoppingcart.Filter;

import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;
import shoppingcart.Exception.ItemNotSameTypeException;
import shoppingcart.Item;

/**
 *
 * @author Hana
 */
public class BuyOneGetOneFilter  extends AbstractFilter {

    @Override
    public double filterPrice(List<Item> items) throws ItemNotSameTypeException {

        super.filterItemsBeforeCalculatePrice(items);

        if (items.size() == 0) {
            return 0;
        }

        AtomicInteger numberOfGroup = new AtomicInteger(items.size() / 2);
        AtomicInteger numberRemain = new AtomicInteger(items.size() % 2);

        return numberOfGroup.get() * items.get(0).getPrice() + numberRemain.get() * items.get(0).getPrice();
    }

    
}
