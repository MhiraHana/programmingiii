/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package shoppingcart.Exception;

/**
 *
 * @author Hana
 */
public class ItemNotSameTypeException extends Exception {

    public ItemNotSameTypeException(String message) {
        super(message);
    }
}
