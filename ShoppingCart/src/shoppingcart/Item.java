/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package shoppingcart;

import shoppingcart.Exception.ItemNotSameTypeException;

/**
 *
 * @author Hana
 */
public class Item {
    private String name;
    private double price;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public Item(String name, double price) {
        this.name = name;
        this.price = price;
    }

    public Item(Item item) throws ItemNotSameTypeException {

        if (item == null) {
            throw new IllegalArgumentException("Item cannot be null.");
        }

        this.name = item.name;
        this.price = item.getPrice();
    }
}
