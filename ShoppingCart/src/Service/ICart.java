/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Service;

import java.util.List;
import shoppingcart.Exception.ItemNotSameTypeException;
import shoppingcart.Item;

/**
 *
 * @author Hana
 */
public interface ICart {
     public List<Item> getItems();// using java 8

    public void empty();

    public void add(String itemName) throws ItemNotSameTypeException;

    public void add(List<String> itemNames) throws ItemNotSameTypeException;

    public double calculateFinalPrice() throws ItemNotSameTypeException;

    public double calculateMarkerPrice() throws ItemNotSameTypeException;
}
