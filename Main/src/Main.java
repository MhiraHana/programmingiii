/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Hana
 */
import java.text.ParseException;
import java.text.SimpleDateFormat;

public class Main {

  public static boolean isValidDate(String inDate) {
    SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
    dateFormat.setLenient(false);
    try {
      dateFormat.parse(inDate.trim());
    } catch (ParseException pe) {
      return false;
    }
    return true;
  }

  public static void main(String[] args) {

    System.out.println(isValidDate("2004-02-29"));
    System.out.println(isValidDate("2005-02-29"));
  }
}