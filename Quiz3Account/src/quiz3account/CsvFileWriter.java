/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package quiz3account;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import static java.lang.System.out;
import java.util.ArrayList;

/**
 *
 * @author 1795845
 */
public class CsvFileWriter {

    //Delimiter used in CSV file
    private static final String COMMA_DELIMITER = ",";
    private static final String NEW_LINE_SEPARATOR = "\n";

    //CSV file header
    private static final String FILE_HEADER = "date,desposit,withDrawal";

    public static void writeCsvFile(ArrayList<Transaction> list, File file) {

     

        FileWriter fileWriter = null;

        try {
            fileWriter = new FileWriter(file.getName());

            //Write the CSV file header
            fileWriter.append(FILE_HEADER.toString());

            //Add a new line separator after the header
            fileWriter.append(NEW_LINE_SEPARATOR);

            //Write a new Transaction object list to the CSV file
            for (Transaction p : list) {

                fileWriter.append(String.valueOf(p.getId()));

                fileWriter.append(COMMA_DELIMITER);

                fileWriter.append(p.getTransactionDate().toString());

                fileWriter.append(COMMA_DELIMITER);

                fileWriter.append(p.getDepositAmount().toPlainString());

                fileWriter.append(COMMA_DELIMITER);

                fileWriter.append(p.getWithdrawalAmount().toPlainString());

                fileWriter.append(NEW_LINE_SEPARATOR);

            }

            System.out.println("CSV file was created successfully !!!");

        } catch (Exception e) {
            System.out.println("Error in CsvFileWriter !!!");
            e.printStackTrace();
        } finally {

            try {
                fileWriter.flush();
                fileWriter.close();
            } catch (IOException e) {
                System.out.println("Error while flushing/closing fileWriter !!!");
                e.printStackTrace();
            }

        }
    }
}
