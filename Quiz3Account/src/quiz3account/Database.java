/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package quiz3account;

import java.io.IOException;
import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.Date;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.ParseException;
import java.util.ArrayList;

/**
 *
 * @author 1795845
 */
public class Database {

    private final static String HOSTNAME = "localhost:3333";
    private final static String DBNAME = "transactiondb";
    private final static String USERNAME = "transactiondb";
    private final static String PASSWORD = "twxUQXevaJpJuZxq";

    private Connection conn;

    public Database() throws SQLException {
        conn = DriverManager.getConnection("jdbc:mysql://" + HOSTNAME + "/" + DBNAME, USERNAME, PASSWORD);

    }

    public void addTransaction(Transaction t) throws SQLException {
        String sql = "INSERT INTO transaction (transactionDate, depositAmount, withdrawalAmount) VALUES (?, ?, ?)";
        try (PreparedStatement stmt = conn.prepareStatement(sql)) {
            stmt.setDate(1, (Date) t.getTransactionDateSql());
            stmt.setBigDecimal(2, t.getDepositAmount());
            stmt.setBigDecimal(3, t.getWithdrawalAmount());

            stmt.executeUpdate();
        }

    }

    public ArrayList<Transaction> getAllTransactions() throws SQLException, ParseException {
        String sql = "SELECT * FROM transaction";
        ArrayList<Transaction> list = new ArrayList<>();
        try (Statement stmt = conn.createStatement()) {
            ResultSet result = stmt.executeQuery(sql);
            while (result.next()) {
                int id = result.getInt("id");
                String DateSql = result.getString("transactionDate");
                BigDecimal depositAmount = result.getBigDecimal("depositAmount");
                BigDecimal withdrawalAmount = result.getBigDecimal("withdrawalAmount");
                Transaction todo = new Transaction(id, DateSql, depositAmount, withdrawalAmount);
                list.add(todo);
            }
            return list;
        }
    }

    public BigDecimal getAccountTotalAmount() throws IOException, SQLException {

        String sql = "SELECT SUM(depositAmount)- SUM(withdrawalAmount) as balance FROM transaction";
        try (Statement stmt = conn.createStatement()) {
            ResultSet result = stmt.executeQuery(sql);
            result.next();
               
            return    result.getBigDecimal("balance");
               
            }
         
        }

    }


