/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package quiz3account;

import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.format.DateTimeFormatter;
import java.util.Calendar;
import java.util.Date;

/**
 *
 * @author 1795845
 */
public class Transaction {

    private int id;
    private Date transactionDate; // type in MySQL is DATETIME
    private BigDecimal depositAmount, withdrawalAmount;
    public static final String DATE_FORMAT_NOW = "yyyy-MM-dd HH:mm:ss";

    public Transaction(int id, String transactionDate, BigDecimal deposit, BigDecimal withdrawal) throws ParseException {
        this.id = id;
        // date current date 
        Calendar cal = Calendar.getInstance();
        SimpleDateFormat sdf = new SimpleDateFormat(DATE_FORMAT_NOW);
        transactionDate = sdf.format(cal.getTime());
        // assign date
        this.transactionDate = sdf.parse(transactionDate);

        // deposit or withdrawal must be null,
        // otherwise throw InvalidArgumentException with message explaining
        this.depositAmount = deposit;
        this.withdrawalAmount = withdrawal;

    }

    public void setId(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }

    public BigDecimal getDepositAmount() {
        return depositAmount;
    }

    public BigDecimal getWithdrawalAmount() {
        return withdrawalAmount;
    }

    public Date getTransactionDate() {
        return transactionDate;
    }

    public java.sql.Date getTransactionDateSql() {
        return new java.sql.Date(transactionDate.getTime());
    }

    @Override
    public String toString() {

        SimpleDateFormat dateFormat = new SimpleDateFormat(DATE_FORMAT_NOW);
        String dateString = dateFormat.format(transactionDate);
        return "Transaction{" + "depositAmount=" + depositAmount + ", withdrawalAmount=" + withdrawalAmount + ", transactionDate=" + dateString + '}';
    }

}
