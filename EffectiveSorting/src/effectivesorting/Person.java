/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package effectivesorting;

import java.util.Comparator;

/**
 *
 * @author 1795845
 */
public class Person implements Comparable<Person> {

    String name;
    int age;

    Person(String name, int age) {
        this.name = name;
        this.age = age;
    }

    public String getName() {
        return name;
    }

    public int getAge() {
        return age;
    }

    @Override
    public int compareTo(Person comparePerson) {
        int compareAge =  comparePerson.getAge();

        //ascending order
        return this.age - compareAge;
    }



    @Override
    public String toString() {
        return "Person{" + "name=" + name + ", age=" + age + '}';
    }
//public static Comparator<Person> AgeComparator = new Comparator<Person>() {
//
//        @Override
//        public int compare(Person p1, Person p2) {
//            return compare(p1,p2);
//        }
//    };
    public static Comparator<Person> NameComparator = new Comparator<Person>() {

        @Override
        public int compare(Person p1, Person p2) {
            return p1.getName().compareTo(p2.getName());
        }
    };
       public static Comparator<Person> NameAgeComparator = new Comparator<Person>() {

        @Override
        public int compare(Person p1, Person p2) {
            //test names first, if names are differest 
            int result= p1.getName().compareTo(p2.getName());
            if(result !=0) return result;
            return p1.age-p2.age;
        }
    };
}
