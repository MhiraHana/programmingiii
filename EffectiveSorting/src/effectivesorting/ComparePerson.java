/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package effectivesorting;

import java.util.ArrayList;
import java.util.Collections;

/**
 *
 * @author 1795845
 */
public class ComparePerson {

    static ArrayList<Person> people = new ArrayList<>();

    public static void main(String args[]) {
        Person p1 = new Person("Jerry", 12);
        people.add(p1);
        Person p2 = new Person("Merry", 20);
        people.add(p2);
        Person p3 = new Person("Tom", 18);
        people.add(p3);
        Person p4 = new Person("Boob", 25);
        people.add(p4);
        Person p5 = new Person("Donald", 30);
        people.add(p5);
        Person p6 = new Person("Jar", 50);
        people.add(p6);
        Person p7 = new Person("Annie", 16);
        people.add(p7);
        Person p8 = new Person("Annie", 20);
        people.add(p8);
        
        System.out.println("*****************Before sorting *************************");

        for (Person p : people) {
            System.out.printf("%s is %d y/o\n", p.name, p.age);
        }

        
        System.out.println("*****************After sorting with comparable *************************");

        Collections.sort(people);

        for (Person p : people) {
            System.out.printf("%s is %d y/o\n", p.name, p.age);
        }

        
         System.out.println("*****************After sorting with comparatorof names*************************");

        Collections.sort(people, Person.NameComparator);

        for (Person p : people) {
            System.out.printf("%s is %d y/o\n", p.name, p.age);
        }
        
         System.out.println("*****************After sorting with name and age  comparator*************************");

        Collections.sort(people, Person.NameComparator);

        for (Person p : people) {
            System.out.printf("%s is %d y/o\n", p.name, p.age);
        }
    }
}
