/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package geometry;

/**
 *
 * @author Hana
 */
public abstract class GeoObj {

    private String color;

    public GeoObj(String color) throws IllegalArgumentException  {
       // Check for a color with a least one letter 
        setColor(color);
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color)  throws IllegalArgumentException {
      if ((color == null)||(color.length()<1)) 
            throw new IllegalArgumentException("Color cannot be null or empty."); 
        this.color = color; 
    }

    @Override
    public String toString() {
        return "GeoObj{" + "color=" + color + '}';
    }

    abstract double getSurface() throws UnsupportedOperationException;

    abstract double getCirccumPerim() throws UnsupportedOperationException;

    abstract int getVerticesCount() throws UnsupportedOperationException;

    abstract int getEdgesCount() throws UnsupportedOperationException;

    abstract void print() throws UnsupportedOperationException;
}
