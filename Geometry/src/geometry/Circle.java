/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package geometry;

/**
 *
 * @author Hana
 */
public class Circle extends GeoObj {

    private double radius;

    public Circle(String color, double radius) {
        super(color);
        if (radius < 0) {
            throw new IllegalArgumentException("radius must be non negative.");
        }
        this.radius = radius;
    }

    public double getRadius() {
        return radius;
    }

    public void setRadius(double radius) {
        this.radius = radius;
    }

    @Override
    double getSurface() throws UnsupportedOperationException {
       return Math.PI * radius * radius;
    }

    @Override
    double getCirccumPerim() throws UnsupportedOperationException {
          return 2.0 * Math.PI * radius;
    }

    @Override
    int getVerticesCount() throws UnsupportedOperationException {
       return 0;
    }

    @Override
    int getEdgesCount() throws UnsupportedOperationException {
       return 0;
    }

    @Override
    void print() throws UnsupportedOperationException {
       System.out.println("Circle;" + super.getColor()+ ";" + getRadius());
    }

}
