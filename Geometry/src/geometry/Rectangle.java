/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package geometry;

/**
 *
 * @author Hana
 */
public class Rectangle extends GeoObj {

    private double width;
    private double height;

    public Rectangle(String color, double width, double height) throws IllegalArgumentException {
        super(color);
        if(width <0 || height<0 ){
        
         throw new IllegalArgumentException("Width and heigth must non negative."); 
        }
        this.width = width;
        this.height = height;
    }

    public double getWidth() {
        return width;
    }

    public void setWidth(double width) {
        this.width = width;
    }

    public double getHeight() {
        return height;
    }

    public void setHeight(double height) {
        this.height = height;
    }

    @Override
    double getSurface() throws UnsupportedOperationException {
        return width * height;
    }

    @Override
    double getCirccumPerim() throws UnsupportedOperationException {
        return 2 * (width + height);
    }

    @Override
    int getVerticesCount() throws UnsupportedOperationException {
        return 4;
    }

    @Override
    int getEdgesCount() throws UnsupportedOperationException {
        return 4;
    }

    //Rectangle;blue;4.5;7.2
    @Override
    void print() throws UnsupportedOperationException {
        System.out.println("Rectangle;" + super.getColor() + ";" + getWidth() + ";" + getHeight());
    }

}
