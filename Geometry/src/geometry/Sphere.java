/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package geometry;

/**
 *
 * @author Hana
 */
public class Sphere extends GeoObj {

    private double radius;

    public Sphere(String color, double radius) {
        super(color);
        this.radius = radius;
    }

    public double getRadius() {
        return radius;
    }

    public void setRadius(double radius) {
        this.radius = radius;
    }

    @Override
    double getSurface() throws UnsupportedOperationException {
        return 4 * Math.PI * radius * radius;
    }

    @Override
    double getCirccumPerim() throws UnsupportedOperationException {
        throw new UnsupportedOperationException("");
    }

    @Override
    int getVerticesCount() throws UnsupportedOperationException {
      return 0;

    }

    @Override
    int getEdgesCount() throws UnsupportedOperationException {
        return 0;
    }

    @Override
    void print() throws UnsupportedOperationException {
        System.out.println("Sphere;" + super.getColor() + ";" + getRadius());
    }

}
