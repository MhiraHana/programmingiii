/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package geometry;

/**
 *
 * @author Hana
 */
public class Hexagon extends GeoObj{

    
    private double edge;
    
    public Hexagon(String color , double edge) {
        super(color);
        this.edge=edge;
    }

    public double getEdge() {
        return edge;
    }

    public void setEdge(double edge) {
        this.edge = edge;
    }

   

    @Override
    double getSurface() throws UnsupportedOperationException {
       return (Math.sqrt(3)/2)*edge*edge;
    }

    @Override
    double getCirccumPerim() throws UnsupportedOperationException {
      return 0.0;
    }

    @Override
    int getVerticesCount() throws UnsupportedOperationException {
           return 6;
    }

    @Override
    int getEdgesCount() throws UnsupportedOperationException {
        return 6;
    }

    @Override
    void print() throws UnsupportedOperationException {
          System.out.println("Hexagon;" + super.getColor()+ ";" + getEdge());
    }
    
}
