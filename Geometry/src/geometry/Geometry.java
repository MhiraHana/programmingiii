/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package geometry;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Scanner;

/**
 *
 * @author Hana
 */
public class Geometry {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here

        try {
            File f = new File("input.txt");
            Scanner sc = new Scanner(f);

            ArrayList<GeoObj> geometry = new ArrayList<>();

            while (sc.hasNextLine()) {
                String line = sc.nextLine();
                try {
                    String[] data = line.split(";");
                    if (data.length < 2) {
                        System.out.println("Warning :Invalid number of data.");
                        continue;
                    }
                   // String color = data.length == 1 ? "" : data[1];
                    if (data.length < 4) {
                        throw new ParsingException("Invalid number of fields, 3 expected");
                    }
                   String forms = data[0];
                   String color = data[1];
                    switch (forms) {
                        case "Rectangle":
                            if (data.length != 4) {
                                throw new ParsingException("Invalid number of fields, 3 expected");
                                
                            }
                            String widthStr = data[2];
                            String heigthStr = data[3];
                            //parsing the string to double 

                            double width = Double.parseDouble(widthStr);
                            double heigth = Double.parseDouble(heigthStr);
                            GeoObj r = new Rectangle(color, width, heigth);
                            geometry.add(r);
                            break;
                        case "Square":
                            if (data.length != 3) {
                                throw new ParsingException("Invalid number of fields, 3 expected");
                            }
                            String sideStr = data[2];
                            String sideStr1 = data[3];

                            //parsing the string to double 
                            double side1 = Double.parseDouble(sideStr);
                            double side2 = Double.parseDouble(sideStr1);
                            GeoObj s = new Square(color, side1, side2);

                            geometry.add(s);
                            break;
                        case "Circle":
                            if (data.length != 3) {
                                throw new ParsingException("Invalid number of fields, 3 expected");
                            }
                            String radiusStr = data[2];

                            //parsing the string to double 
                            double radius = Double.parseDouble(radiusStr);
                            GeoObj c = new Circle(color, radius);

                            geometry.add(c);
                            break;
                        case "Point":
                            if (data.length != 2) {
                                throw new ParsingException("Invalid number of fields, 3 expected");
                            }

                            GeoObj p = new Point(color);

                            geometry.add(p);
                            break;
                        case "Sphere":
                            if (data.length != 3) {
                                throw new ParsingException("Invalid number of fields, 3 expected");
                            }
                            String radiusSt = data[2];

                            //parsing the string to double 
                            double rad = Double.parseDouble(radiusSt);
                            GeoObj sp = new Sphere(color, rad);

                            geometry.add(sp);
                            break;

                        case "Hexagon":
                            if (data.length != 3) {
                                throw new ParsingException("Invalid number of fields, 3 expected");
                            }
                            String edgeSt = data[2];

                            //parsing the string to double 
                            double edge = Double.parseDouble(edgeSt);
                            GeoObj h = new Hexagon(color, edge);

                            geometry.add(h);
                            break;

                        default:
                            System.out.println("Invalid forms....");
                            break;

                    }
        
                } catch (IllegalArgumentException | ParsingException e) {
                    System.out.println("Error parsing line: " + line);
                    System.out.println("Reason: " + e.getMessage());
                }
            }
            System.out.println("****************************************************** ");
            for (GeoObj g : geometry) {
                g.print();
                
            }

        } catch (IOException e) {
            /// e.printStackTrace();
            System.out.println("File reading error: " + e.getMessage());
        }
    }

}

class ParsingException extends Exception {

    public ParsingException() {
        super();
    }

    public ParsingException(String message) {
        super(message);
    }

    public ParsingException(String message, Throwable cause) {
        super(message, cause);
    }
}
