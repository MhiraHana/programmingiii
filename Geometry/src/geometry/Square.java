/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package geometry;

/**
 *
 * @author Hana
 */
public class Square extends Rectangle {

    private double side;

    public Square(String color, double width, double height) {
        super(color, width, height);
    }

    public Square(String color,double side) {
        super(color, side, side);

    }

    public double getSide() {
        return side;
    }

    public void setSide(double side) {
        super.setWidth(side);
        super.setHeight(side);
    }
 @Override
    void print() throws UnsupportedOperationException {
      System.out.println("Square;" + super.getColor()+ ";" + getSide());
    }
}
