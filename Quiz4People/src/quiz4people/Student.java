/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package quiz4people;

import java.math.BigDecimal;

/**
 *
 * @author 1795845
 */
public class Student extends Person {

    String program; // 2 or more characters
    BigDecimal gpa; // from 0 to 4.0

    public Student(String name, int age, String program, BigDecimal gpa) throws InvalidDataException {
        super(name, age);
        setProgram(program);
        setGpa(gpa);
    }

    public String getProgram() {

        return program;
    }

    public void setProgram(String program) throws InvalidDataException {
        if (program == null || program.length() < 2) {
            throw new InvalidDataException("Name must not be empty and be more than 2 characters.");
        }
        this.program = program;
    }

    public BigDecimal getGpa() {
        return gpa;
    }

    public void setGpa(BigDecimal gpa) throws InvalidDataException {
        BigDecimal bg1, bg2;
        bg1 = new BigDecimal("0");
        bg2 = new BigDecimal("4");

        if ((gpa.compareTo(bg1) == -1) && (gpa.compareTo(bg2) == -1)) {
            throw new InvalidDataException("gpa must  be between 0.0 and 4.0.");
        }
        this.gpa = gpa;
    }

    @Override
    public void print() {
        System.out.println(String.format("Student : %s  is %d y/o, has program %s and  gpa %.2f .", getName(), getAge(), getProgram(), getGpa()));
    } // display ALL fields of this class

    @Override
    public int compareTo(Person o) {
        return this.getName().compareTo(o.getName());
    }

}
