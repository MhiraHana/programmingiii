/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package quiz4people;

import java.io.File;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.InputMismatchException;
import java.util.Scanner;

/**
 *
 * @author 1795845
 */
public class Quiz4People {

    static ArrayList<Person> people = new ArrayList<>();

    static final String FILE_NAME = "input.txt";

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws InvalidDataException {
        // TODO code application logic here

        try {
            Scanner fileInput = new Scanner(new File(FILE_NAME));
            while (fileInput.hasNextLine()) {
                try {
                    String line = fileInput.nextLine();
                    System.out.println("Processing line: " + line);
                    String[] data = line.split(";");
                   
                    if (data.length < 2) {
                        throw new InvalidDataException("Warning: Invalid number of data in line " + line);
                    }
                    String name = data[1];
                    String ageStr = data[2];

                    switch (data[0]) {
                        case "Student": {
                            if (data.length != 5) {
                                System.out.println("Warning: Invalid number of data in line " + line);
                                continue;
                            }

                            int age = Integer.parseInt(ageStr);

                            BigDecimal gpa = new BigDecimal(data[4]);
                            people.add(new Student(name, age, data[3], gpa));
                        }
                        break;
                        case "Teacher": {
                            if (data.length != 5) {
                                System.out.println("Warning: Invalid number of data in line " + line);
                                continue;
                            }

                            int age = Integer.parseInt(ageStr);
                            int nbreYears = Integer.parseInt(data[4]);
                            people.add(new Teacher(name, age, data[3], nbreYears));
                        }
                        break;

                        default:
                            System.out.println("Warning: invalid data in line " + line);
                    }
                } catch (NumberFormatException | InvalidDataException e) {
                    System.out.println("Warning: invalid data " + e.getMessage());

                }
            } // loop end
            fileInput.close();
        } catch (IOException e) {
            System.err.println("Error reading file");
        } catch (InputMismatchException e) {
            System.err.println("Error: file contents mismatch");
        }

        // 2. print them out
        System.out.println();
        System.out.println("********************************************************************");
        for (Person p : people) {
            System.out.println();
            p.print();
            System.out.println();
        }
        System.out.println();
        System.out.println("*****************After  sorting by name *************************");

        Collections.sort(people);

        for (Person p : people) {
            System.out.println();
            p.print();
            System.out.println();
        }
        System.out.println();
       
        System.out.println("*****************After  sorting by compareByAge *************************");

        Collections.sort(people, Person.compareByAge);

        for (Person p : people) {
            System.out.println();
            p.print();
            System.out.println();
        }
        System.out.println();

    }
}
