/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package quiz4people;

import java.util.Comparator;

/**
 *
 * @author 1795845
 */
class InvalidDataException extends Exception {

    InvalidDataException(String message) {
        super(message);
    }
}

abstract class Person implements Comparable<Person> ,Printable  {

    private String name; // 2 or more characters
    private int age; // 0-150

    public Person(String name, int age) throws InvalidDataException {
        setName(name);
        setAge(age);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) throws InvalidDataException {
        if (name == null || name.length() < 2) {
            throw new InvalidDataException("Name must not be empty and be more than 2 characters.");
        }
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) throws InvalidDataException {
        if (age < 0 || age > 150) {
            throw new InvalidDataException("Age must  be between 0 and 150.");
        }
        this.age = age;
    }

    @Override
    abstract public void print();

    public int compare(Person o1) {
        return this.name.compareTo(o1.name);
    }

    public static Comparator<Person> compareByAge = new Comparator<Person>() {

        @Override
         public int compare(Person o1, Person o2) {
            
            return o1.age - o2.age;
        }
    };

}
