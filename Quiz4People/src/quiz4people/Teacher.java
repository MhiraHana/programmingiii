/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package quiz4people;

/**
 *
 * @author 1795845
 */
public class Teacher extends Person {

    String subject; // 2 or more characters
    int yearsOfExperience;

    public Teacher(String name, int age, String subject, int yearsOfExperience) throws InvalidDataException {
        super(name, age);
        setSubject(subject);
        setYearsOfExperience(yearsOfExperience);
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) throws InvalidDataException {
        if (subject == null || subject.length() < 2) {
            throw new InvalidDataException("subject must not be empty and be more than 2 characters.");
        }
        this.subject = subject;
    }

    public int getYearsOfExperience() {
        return yearsOfExperience;
    }

    public void setYearsOfExperience(int yearsOfExperience) {
        this.yearsOfExperience = yearsOfExperience;
    }

    @Override
    public void print() {
       System.out.println(String.format("Teacher: %s  is %d y/o, has subject %s and  years Of Experience %d .", getName(), getAge(), getSubject(), getYearsOfExperience()));
        
    } // display ALL fields of this class


    @Override
    public int compareTo(Person o) {
       return this.getName().compareTo(o.getName());
    }
   

}
