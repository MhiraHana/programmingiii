/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package peopleList;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.InputMismatchException;
import java.util.Scanner;

/**
 *
 * @author 1795845
 */
public class FileStorage {

    public static final String FILE_NAME = "dataPeople.txt";
// add File file parameter in the two function 
    static void savePeopleToFile(ArrayList<Person> list) throws IOException {
        
        try ( // FIXME: use try-with-resources here
                PrintWriter out = new PrintWriter(new BufferedWriter(new FileWriter(FILE_NAME)))) {
            for (Person p : list) {
                out.println(p.getName());
                out.println(p.getAge());
                out.println(p.getCodePostal());
            }
        }
    }

    static ArrayList<Person> loadPeopleFromFile() throws IOException {
        ArrayList<Person> list = new ArrayList<>();
        try {
            File file = new File(FILE_NAME);
            if (!file.exists()) {
                return list;
            }
            Scanner in = new Scanner(file);
            while (in.hasNextLine()) {
                String name = in.nextLine();
                int age = in.nextInt();
                in.nextLine(); // consume the left-over newline character
                String codePostal=in.nextLine();
                Person p = new Person(name, age,codePostal);
                list.add(p);
            }
        } catch (InputMismatchException e) {
            throw new IOException("Error while parsing data file", e);
        } catch (IllegalArgumentException e) {
            throw new IOException("Error while parsing data file", e);
        }
        return list;
    }
}
