/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package peopleList;

/**
 *
 * @author 1795845
 */
public class Person {

    private String name;
    private int age;
    private String codePostal;

    public Person(String name, int age, String codePostal) {
        setName(name);
        setAge(age);
        setCodePostal(codePostal);
    }

    public String getName() {
        return name;
    }

    public final void setName(String name) {
        if (name == null) {

            throw new IllegalArgumentException("Name must not be null");

        }
        if (name.length() < 2 || name.length() > 20) {

            throw new IllegalArgumentException("Name must 2-20 characters long");
        }

        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public final void setAge(int age) {
        if (age < 1 || age > 150) {
            throw new IllegalArgumentException("Age must be between 1-150");
        }

        this.age = age;
    }

    public String getCodePostal() {

        return codePostal;
    }

    public final void setCodePostal(String codePostal) {
        String pattern = "([a-z][0-9][a-z] ?[0-9][a-z][0-9])";
        
        if (!codePostal.toLowerCase().matches(pattern)) {

            throw new IllegalArgumentException("the postal code must be in this format h3m1e8");

        }

        if (codePostal.length() > 6) {
            throw new IllegalArgumentException("the length of postal code must be 6 caracters.");
        }
        this.codePostal = codePostal;
    }

    @Override
    public String toString() {

        return String.format("%s is %d y/o , her code postal is %s ", name, age, codePostal);

    }
}
