/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package genplayteacher;

import java.util.ArrayList;

/**
 *
 * @author 1795845
 */
public class Stack<T> {

    ArrayList<T> array = new ArrayList();

    public Stack() {
        
    }

    public int getHeight() {
        return array.size();
    }

    public void push(T item) {
        // insert  new element at the biginning of the arraylist
        array.add(0, item);
    }

    public T pop() {
        //remove element from the biginning  of array
        
        if(array.isEmpty()){return null;}
        return array.remove(0);
        
       
    }
}
