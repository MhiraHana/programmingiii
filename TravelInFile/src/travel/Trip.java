/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package travel;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 *
 * @author 1795845
 */
public class Trip {

    private String destination;
    private String name;
    private String passeportNo;
    private Date departure;
    private Date returner;

    public Trip(String destination, String name, String passeportNo, String departure, String returner) throws ParseException {
        setDestination(destination);
        setName(name);
        setPasseportNo(passeportNo);
        setDate(departure, returner);
    }

    public String getDestination() {
        return destination;
    }

    public final void setDestination(String destination) {

        if (destination == null) {

            throw new IllegalArgumentException("Destination must not be null");

        }
        if (destination.length() < 1 || destination.length() > 50) {

            throw new IllegalArgumentException("Destination must 1-50 characters long");
        }

        this.destination = destination;
    }

    public String getName() {
        return name;
    }

    public final void setName(String name) {

        if (name == null) {

            throw new IllegalArgumentException("Name must not be null");

        }
        if (name.length() < 1 || name.length() > 50) {

            throw new IllegalArgumentException("Name must 1-50 characters long");
        }

        this.name = name;
    }

    public String getPasseportNo() {
        return passeportNo;
    }

    public final void setPasseportNo(String passeportNo) {
        String pattern = "([A-Z][A-Z][0-9][0-9][0-9][0-9][0-9][0-9])";

        if (!passeportNo.matches(pattern)) {

            throw new IllegalArgumentException("The passport No must be in this format AB123456");

        }

        if (passeportNo.length() > 8) {
            throw new IllegalArgumentException("The length of passport No must be 8 caracters.");
        }
        this.passeportNo = passeportNo;
    }

    public Date getDeparture() {
        return departure;
    }

    public final void setDate(String departure, String returner) throws ParseException {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        Date depart = dateFormat.parse(departure.trim());
        Date retur = dateFormat.parse(returner.trim());
        if (depart.compareTo(retur) > 0) {
            throw new IllegalArgumentException("You must enter date departure befor date return ");
        }
        setDeparture(departure);
        setReturner(returner);
    }

    public final void setDeparture(String departure) throws ParseException {
        if (!isValidDate(departure)) {

            throw new IllegalArgumentException("You must enter string in this format yyyy-MM-dd ");
        }
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");

        this.departure = dateFormat.parse(departure.trim());
    }

    public Date getReturner() {
        return returner;
    }

    public final void setReturner(String returner) throws ParseException {
        if (!isValidDate(returner)) {

            throw new IllegalArgumentException("You must enter string in this format yyyy-MM-dd ");
        }
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");

        this.returner = dateFormat.parse(returner.trim());
    }

    public static boolean isValidDate(String inDate) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        dateFormat.setLenient(false);
        try {
            dateFormat.parse(inDate.trim());

        } catch (ParseException pe) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        String dateString = dateFormat.format(departure);
        String dateRetStr = dateFormat.format(returner);
        return String.format("%s (%s) travels to %s in date %s and will arrive at  %s", name, passeportNo, destination, dateString, dateRetStr);

    }
}
