/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package travel;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

/**
 *
 * @author 1795845
 */
public class FileStorage {


    static void saveTripToFile(ArrayList<Trip> list, File file) throws IOException {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");

        String pathToFile = file.getAbsolutePath();
     
        try (PrintWriter out = new PrintWriter(new BufferedWriter(new FileWriter(pathToFile, true)))) {
            String line = null;
            for (Trip p : list) {
                String dest = p.getDestination();
                String name = p.getName();
                String passportNo = p.getPasseportNo();
                Date dep = p.getDeparture();
                Date returner = p.getReturner();
                String dateString = null;
                dateString = dateFormat.format(dep);
                String dateRetStr = dateFormat.format(returner);

                line = String.format("%s;%s;%s;%s;%s", dest, name, passportNo, dateString, dateRetStr);
                System.out.println(line);
                out.println(line);
            }
        }
    }
    
    
     private static String getFileExtension(File file) {
        String fileName = file.getName();
        if (fileName.lastIndexOf(".") != -1 && fileName.lastIndexOf(".") != 0) {
            return fileName.substring(fileName.lastIndexOf(".") + 1);
        } else {
            return "";
        }
    }
}


